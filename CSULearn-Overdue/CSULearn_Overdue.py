### Reference: https://www.freecodecamp.org/news/send-emails-using-code-4fcea9df63f/

### Completely linear in nature


import smtplib

from string import Template

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from os.path import expandvars
from getpass import getpass

mailhost='smtp.office365.com'
mailport=587

template_dir=expandvars('%APPDATA%\\Microsoft\\Templates\\')
template_file='Overdue Compliance Assignment.htm'

contacts_dir=expandvars('%USERPROFILE%\\Desktop\\')
contacts_file='Contacts.csv'

def getInput(st):
    ret=None
    try:
        try:
            ret=input(st)
        except NameError:
            ret=raw_input(st)
        except KeyboardInterrupt:
            raise
    except KeyboardInterrupt:
        return None
    finally:
        return ret

def get_contacts(filename=contacts_dir+contacts_file):
    """
    Return 6 lists from the dump provided read from a file specified by filename.
    potential fields:
    Activity Name	User Number	CSUB ID	Last Name	First Name	Full Name	Email	Department	Job	Employee Type	HR Manager Name	Communication Manager Name	Communication Manager Email	Communication Manager Title	Communication Manager Department	Requirement Status	Due Date	Date Assigned	User Start Date	Reports To Manager Full Name	Reports To Manager Email	Satisfied	Activity Code	Is Certification	Is Required

    """
    
    target_first_names = [] # 4
    target_last_names = [] # 3
    activity_names = [] # 0
    target_emails = [] # 7
    manager_emails = [] # 13
    due_dates = [] # 17

    try:
        with open(filename, mode='r', encoding='utf-8') as contacts_file:
            next(contacts_file) # skip header row
            for a_contact in contacts_file:
                linedata=a_contact.split(',')
                target_first_names.append(linedata[4])
                target_last_names.append(linedata[3])
                activity_names.append(linedata[0])
                target_emails.append(linedata[7])
                manager_emails.append(linedata[13])
                due_dates.append(linedata[17])
    except KeyboardInterrupt:
        return None,None,None,None,None,None

    return target_first_names, target_last_names, activity_names, target_emails, manager_emails, due_dates

def read_template(filename=template_dir+template_file):
    """
    Returns a Template object comprising the contents of the 
    file specified by filename.
    """
    
    with open(filename, 'r', encoding='utf-8') as template_file:
        template_file_content = template_file.read()
    return Template(template_file_content)

def main():
    first_names, last_names, activity_names, emails, manager_emails, due_dates = get_contacts()
    if not first_names:
        print('An error occured during contact enumeration, or there was no one to contact.')
        return 0
    message_template = read_template()
    if not message_template:
        print('An error occured during message template ingestion')
        return 1

    # set up the SMTP server
    s = smtplib.SMTP(host=mailhost, port=mailport)
    s.starttls()

    login = getInput('Enter your login email address for {} (erichards1@csub.edu): '.format(mailhost)) or 'erichards1@csub.edu'
    pwd=getpass('Enter your password for {}@{}:{}: '.format(login,mailhost,mailport))
    from_address=getInput('Enter the sender address (CSULearn@csub.edu): ') or 'CSULearn@csub.edu'

    s.login(login,pwd)

    login='x'*128
    pwd='x'*128
    login=None
    pwd=None

    # For each contact, send the email:
    for first_name, last_name, activity_name, email, manager_email, due_date in zip(first_names, last_names, activity_names, emails, manager_emails, due_dates):
        if not email or len(email) < 3:
            continue
        msg = MIMEMultipart()       # create a message

        # add in the actual person name to the message template
        message = message_template.substitute(first_name=first_name,last_name=last_name,course_name=activity_name,due_date=due_date)

        # Prints out the message body for our sake
        print(message)
        print(email)
        print(manager_email)

        # setup the parameters of the message
        msg['From']=from_address
        msg['To']=email
        msg['Cc']=manager_email
        msg['Subject']="Overdue Compliance Assignment for {} {}".format(first_name,last_name)
        
        # add in the message body
        msg.attach(MIMEText(message, 'html'))
        
        # send the message via the server set up earlier.
        s.send_message(msg)
        del msg
        
    # Terminate the SMTP session and close the connection
    s.quit()
    
if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('Bye!')
